package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"github.com/adrg/xdg"
	"github.com/cloudflare/cloudflare-go"
	"github.com/robfig/cron"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"github.com/urfave/cli/v2/altsrc"
)

type Ddns struct {
	ApiToken string
	ZoneID   string
	Domains  []string
}

// Run DDNS client once.
func (ddns *Ddns) Run() error {
	api, err := cloudflare.NewWithAPIToken(ddns.ApiToken)

	if err != nil {
		return fmt.Errorf("Failed to authorize cloudflare client. %w", err)
	}

	logrus.Info("Trying to get public ip address")

	ipv4, err := getIP(false)
	if err != nil {
		logrus.WithError(err).Debug("Failed to get IPV4 address")
	}

	ipv6, err := getIP(true)
	if err != nil {
		logrus.WithError(err).Debug("Failed to get IPV6 address")
	}

	if ipv4 == "" && ipv6 == "" {
		return fmt.Errorf("Failed to get both ipv4 and ipv6 address. Is the network up?")
	}

	logrus.WithFields(logrus.Fields{
		"ipv4": ipv4,
		"ipv6": ipv6,
	}).Info("Got host ip address(es)")

	logrus.WithField("ZoneID", ddns.ZoneID).Info("Listing DNS records of given zone")

	records, err := api.DNSRecords(ddns.ZoneID, cloudflare.DNSRecord{})
	if err != nil {
		return fmt.Errorf("Failed to list DNS record for given zone. %w", err)
	}

	domainMap := make(map[string]struct{})
	for _, domain := range ddns.Domains {
		domainMap[domain] = struct{}{}
	}

	for _, r := range records {
		logrus.WithFields(logrus.Fields{
			"Name":    r.Name,
			"Content": r.Content,
			"Type":    r.Type,
		}).Debug("Operating on DNS record")

		_, exist := domainMap[r.Name]
		if !exist {
			continue
		}

		if r.Type == "A" && ipv4 != "" && r.Content != ipv4 {
			logrus.WithFields(logrus.Fields{
				"Domain":     r.Name,
				"Type":       r.Type,
				"OldContent": r.Content,
				"NewContent": ipv4,
			}).Info("Updating DNS record for the domain")

			err := api.UpdateDNSRecord(ddns.ZoneID, r.ID, cloudflare.DNSRecord{
				Content: ipv4,
			})

			if err != nil {
				logrus.WithError(err).Warn("Failed to update DNS record")
			} else {
				logrus.WithField("Domain", r.Name).Info("DNS update committed")
			}
		}

		if r.Type == "AAAA" && ipv6 != "" && r.Content != ipv6 {
			logrus.WithFields(logrus.Fields{
				"Domain":     r.Name,
				"Type":       r.Type,
				"OldContent": r.Content,
				"NewContent": ipv6,
			}).Info("Updating DNS record for the domain")

			err := api.UpdateDNSRecord(ddns.ZoneID, r.ID, cloudflare.DNSRecord{
				Content: ipv6,
			})

			if err != nil {
				logrus.WithError(err).Warn("Failed to update DNS record")
			} else {
				logrus.WithField("Domain", r.Name).Info("DNS update committed")
			}
		}
	}

	logrus.Info("Update completed")

	return nil
}

// getIP tries to get IP address from ipify.org API.
//
// If parameter v6 is true, this will attempt to get an ipv6 address.
// Otherwise this will try to get an ipv4 address.
func getIP(v6 bool) (string, error) {
	url := "https://api.ipify.org"
	if v6 {
		url = "https://api6.ipify.org"
	}

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(ip), nil
}

func main() {
	defaultCfgPath := path.Join(xdg.ConfigHome, "cf-ddns", "cf-ddns.toml")

	var ddns Ddns

	var loglevel string

	var schedule string

	var now bool

	app := cli.NewApp()
	app.Usage = "Dynamic DNS client for cloudflare"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:  "config",
			Usage: "TOML config file path",
			Value: defaultCfgPath,
		},
		altsrc.NewStringFlag(&cli.StringFlag{
			Name:        "loglevel",
			Usage:       "Set log level for logger. Available levels are panic, fatal, error, warn, info, debug and trace",
			Destination: &loglevel,
			Value:       "INFO",
		}),
		altsrc.NewBoolFlag(&cli.BoolFlag{
			Name:        "now",
			Usage:       "If true, run ddns now instead of waiting for the next schedule",
			Destination: &now,
			Value:       true,
		}),
		altsrc.NewStringFlag(&cli.StringFlag{
			Name:        "cron",
			Usage:       "Cron schedule for running the ddns. See https://pkg.go.dev/github.com/robfig/cron#hdr-CRON_Expression_Format for available formats",
			Destination: &schedule,
			Value:       "@hourly",
		}),
		altsrc.NewStringFlag(&cli.StringFlag{
			Name:        "api_token",
			Usage:       "Cloudflare API token",
			Destination: &ddns.ApiToken,
		}),
		altsrc.NewStringFlag(&cli.StringFlag{
			Name:        "zone_id",
			Usage:       "Domain zone ID",
			Destination: &ddns.ZoneID,
		}),
		altsrc.NewStringSliceFlag(&cli.StringSliceFlag{
			Name:  "domains",
			Usage: "List of domains",
		}),
	}

	app.Before = altsrc.InitInputSourceWithContext(app.Flags, altsrc.NewTomlSourceFromFlagFunc("config"))

	app.Action = func(c *cli.Context) error {
		// HACK: cli is working weird for Destination+StringSliceFlag. We patch the value here
		// https://github.com/urfave/cli/issues/1143
		ddns.Domains = c.StringSlice("domains")

		level, err := logrus.ParseLevel(loglevel)
		if err != nil {
			logrus.WithError(err).Warn("Unknown log level. See --help for available levels")
		} else {

			logrus.SetLevel(level)
		}

		logrus.WithField("config", ddns).Info("Read configuration")

		// Schdule cron job first to check `--schedule` flag asap
		cr := cron.New()
		err = cr.AddFunc(schedule, func() {
			err := ddns.Run()
			if err != nil {
				logrus.WithError(err).Warn("Failed to run ddns")
			}
		})

		if err != nil {
			return err
		}

		if now {
			err := ddns.Run()
			if err != nil {
				logrus.WithError(err).Warn("Failed to run ddns")
			}
		}

		logrus.WithField("schedule", schedule).Info("Starting cron scheduler")
		cr.Run()

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal()
		return
	}
}
