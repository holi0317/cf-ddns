FROM alpine:latest

# Install cf-ddns
COPY cf-ddns /
ENTRYPOINT ["/cf-ddns"]
