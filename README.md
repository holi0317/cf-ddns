# Cloudflare DDNS

A dynamic DNS client for cloudflare.

Note: This project is not an official product from cloudflare nor it has any
affiliation with cloudflare.

# Supported features

- Update DNS A/AAAA records in cloudflare DNS service with host ip address
- Supports both ipv4 and ipv6
- Docker image available
- Multiple domains under same zone

Note: IPV6 code has not been tested as I (still) don't have an IPV6 address

# Limitations

## 1 zone per configuration

Only 1 zone per configuration file is supported. If there are multiple zones you
would like to update, run the program multiple times and specify another configuration file.

Multiple domains that live in the same zone can be updated in 1 run.

## DNS record will not be created

This program only do update on existing DNS record. Which means following scenario
will not work:

1. Specifying a domain in configuration file that does not have A/AAAA record in cloudflare
2. Have IPV6 address but did not add AAAA record for specified domain

# Installation

Note: Only linux/amd64 platform has been tested. We are building for other platforms
but they may not work. Open an issue if this broke on other platforms.

This project can run with or without docker


## Run with docker

(Only linux/amd64 image is published. Open an issue if you want to run on other platforms.)

The docker image includes a cron implementation which will by default run in foreground.
It is expected to run this container and let it do ddns logic periodically.

Run following script to start it. Assuming the configuration file is in `./cf-ddns.toml`:

```bash
docker run --rm -e SCHEDULE="* * * * *" -v `pwd`/cf-ddns.toml:/cf-ddns.toml registry.gitlab.com/holi0317/cf-ddns
```

This should run the image every minute for debugging purpose. By default, the schedule is every hour.

The configuration file should be placed in `/cf-ddns.toml` in the container.

Note: The image is not published to docker hub but gitlab container registry instead.

### Tags

Only major version and latest tag will be published. It is recommended to pin to the major version.

### Configuration 

Following configuration can be applied via environment variable

#### SCHEDULE

CRON string for how often this script to run. Default to `0 * * * *` (Every hour)

#### CF_DDNS_CFG_FILE

Path to the configuration file. Default to `/cf-ddns.toml`

## Run compiled binary

Every release contains binary. See [release page] for the binary.

[release page]: https://gitlab.com/holi0317/cf-ddns/-/releases

## Build from source

With go >= 1.15 installed, run following:

```bash
go build -o cf-ddns
```

# Setup and Configuration

Make sure your site (domain) is in cloudflare before proceeding.

## 1. Get an API token for updating DNS records

Go to [API token page]. Create a token with "Edit zone DNS" template. Include the
zone you would like to target on. The token configuration should be similar to the
following image:

![Token configuration page on cloudflare](./docs/token.png)

Copy the token and save it somewhere. It will be needed when writing
the configuration file.

[API token page]: https://dash.cloudflare.com/profile/api-tokens

## 2. Get target zone ID

Go to [Cloudflare dashboard]. Click the zone you want to edit. In that dashboard,
there should be a "Zone ID" after scrolling down a bit.

Copy the zone ID and save it somewhere. It will be needed when writing
the configuration file.

[Cloudflare dashboard]: https://dash.cloudflare.com

## 3. Add DNS records

This script only does update to DNS records. It will not create any new DNS records.

For example, if you want to do ddns on a domain a.example.com, you need to first
create DNS A record (and AAAA record, if you have IPV6 address) in cloudflare dashboard
before running this script.

## 4. Write configuration file

The configuration file is in [toml] format. The following is a template for creating
the configuration file:

[toml]: https://toml.io/en/

```toml
api_token = ""
zone_id = ""
domains = ["example.com", "a.example.com"]
```

Save the file to `$XDG_CONFIG_HOME/cf-ddns/cf-ddns.toml`.
Which is `~/.config/cf-ddns/cf-ddns.toml` on Linux. Run the script with `-h`
to see the default on different platforms.

The file can also be specified in command line flag `-config`.
