.PHONY: build
build:
	goreleaser build --snapshot --rm-dist --single-target --output dist/cf-ddns

.PHONY: build-all
build-all:
	goreleaser build --snapshot --rm-dist

.PHONY: test
test:
	go test ./...

.PHONY: lint
lint:
	golangci-lint run -v

.PHONY: clean
clean:
	rm -rf dist
