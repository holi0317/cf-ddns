module gitlab.com/holi0317/cf-ddns

go 1.17

require (
	github.com/adrg/xdg v0.3.0
	github.com/cloudflare/cloudflare-go v0.13.7
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.2.0
	github.com/urfave/cli/v2 v2.3.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65 // indirect
	gopkg.in/yaml.v2 v2.2.3 // indirect
)
